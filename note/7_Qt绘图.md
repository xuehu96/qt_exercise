# Qt绘图

画家QPainter -> QPaintEngine -> 绘图设备QPaintDevice


## 画图
在窗口绘图
>10_PaintEvent
重写PaintEvent （注意写参数，重写虚函数，不是重载）

## 手动刷新
update();


## 绘图设备
>12_QPixmap

- QPixmap： 针对屏幕进行优化 和平台相关，不能对图片进行修改
- QImage： 和平台无关，可以对图片进行修改，可以在线程里绘图
- QPicture：保存绘图的状态（二进制文件）

前两个保存出来的是图片，第三个保存出来是二进制

### 绘图设备相互转换
```c++
    //pixmap转image
    pixmap.load("../1.jpg");
    QImage tempImage = pixmap.toImage();
    //image转 QPiaxmap
    QPixmap tempPixmap = QPixmap::fromImage(tempImage);
```

### 不规则窗口
1. 给窗口画一张背景图‘
2. 去窗口边框
2. 设置背景透明
4. 重写移动（移动坐标是相对于屏幕，是以左上角移动）
```c++
//构造函数
去窗口边框
setWindowFlags(Qt::FramelessWindowHint | windowFlags() );

//设置透明
setAttribute(Qt::WA_TranslucentBackground);


//重写paintEvent
设置背景图片

//重写鼠标按下和释放 用于移动窗口
求相对坐标
```