# Qt样式表
qss

>QWiget::setStyleSheet()  
QApplication::setStyleSheet()  
可以为一个独立的子部件、整个窗口、整个应用程序指定一个样式表

```css
selector{attribute: value}
```
- 选择器
- 属性

## 属性
- color 前景色
- background-color 背景色
- background-image:url(://img.jpg)
- border-image:url(://img.jpg)
- background-position和background-repeat