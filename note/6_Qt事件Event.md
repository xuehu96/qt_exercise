# Qt事件
所有事件类都继承于QEvent

app.exec 等待事件的发生

事件过滤器eventFilter ->  分发器event() -> 事件处理函数 ☆ 


## 自定义控件的新建方法
1. add new c++class
2. 继承QWidget
3. 修改3处
    - 头文件2处QWidget改为QLabel
    - .cpp  冒号后面 QWidget改为QLabel
4. 重写protected函数


## 事件
- 鼠标事件 #include <QMouseEvent>
- 键盘事件 #include <QKeyEvent>
- 定时器事件 QObject

## 事件的接收和忽略
```c++
//鼠标事件
protected:
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);

    void leaveEvent(QEvent *event);
    void enterEvent(QEvent *event);

//键盘和定时器事件
protected:
    //键盘按下事件
    void keyPressEvent(QKeyEvent *event);
    //定时器事件
    void timerEvent(QTimerEvent *event);

/* Alt+Enter 生成代码 */

MyLabel::MyLabel(QWidget *parent) : QLabel(parent)
{
    //设置默认追踪鼠标
    this->setMouseTracking(true);
}

void MyLabel::mousePressEvent(QMouseEvent *ev)
{
    int a = ev->x();
    int b = ev->y();

    QString str = QString("<center><h1>press = (%1,%2)</h1></center>").arg(a).arg(b);
    this->setText(str);

    //判断是左键还是右键
    if(ev->button()==Qt::LeftButton)
    {
        qDebug()<<"左键按下";
    }

}

void MyLabel::mouseReleaseEvent(QMouseEvent *ev)
{
    int a = ev->x();
    int b = ev->y();

    QString str = QString("<center><h1>release = (%1,%2)</h1></center>").arg(a).arg(b);
    this->setText(str);


}

void MyLabel::mouseMoveEvent(QMouseEvent *ev)
{
    int a = ev->x();
    int b = ev->y();

    QString str = QString("<center><h1>move = (%1,%2)</h1></center>").arg(a).arg(b);
    this->setText(str);

}

void MyLabel::leaveEvent(QEvent *event)
{
    qDebug()<<"离开";
}

void MyLabel::enterEvent(QEvent *event)
{
    qDebug()<<"进入";
}

MyWidget::MyWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MyWidget)
{
    ui->setupUi(this);

    //单位毫秒
    int timer_id = this->startTimer(1000);
    //停止定时器
//    this->killTimer(timer_id);
}

MyWidget::~MyWidget()
{
    delete ui;
}

void MyWidget::keyPressEvent(QKeyEvent *event)
{
    qDebug()<<event->key();
    if(event->key() == Qt::Key_A)
    {
       qDebug()<<"keyA";
    }
}

void MyWidget::timerEvent(QTimerEvent *event)
{
    static int sec = 0;
    sec++;
    ui->label->setText(QString("%1").arg(sec));

//    if(event->timerId() == timer_id)
//    判断是哪个定时器
}

```
## event（）函数  消息分发
`#include <QEvent>`

> QWidget -> event()

不要轻易改event函数，return错 会让事件失效

## 事件过滤器
QObject eventFilter()
```c++
virtual bool QObject::eventFilter(QObject *watched,QEvent* event)

//不想继续转发 返回true 否则false
```

> 事件过滤器必须和被安装过滤器的组件在同一个线程 ，否则 过滤器不起作用