# Qt文件操作
- 传统模式 参数char* int  返回值长度
- Qt模式 返回值是QByteArray

```c++
void Widget::on_button_Read_clicked()
{
    QString path = QFileDialog::getOpenFileName(this,"open","../","TXT(*.txt)");

    if(path.isEmpty() == false)
    {
        QFile file(path);
        //打开 只读方式
        bool isOK = file.open(QIODevice::ReadOnly);
        if(isOK == true)
        {
            //读文件 显示到窗口 默认utf8
            QByteArray array = file.readAll();
            //显示到编辑区
            ui->textEdit->setText(array);
            file.close();
        }
    }
}

void Widget::on_button_Write_clicked()
{
    //选择保存路径
    QString path = QFileDialog::getSaveFileName(this,"save","../","txt(*.txt)");
    if(path.isEmpty() == false)
    {
        QFile file;//创建文件对象
        //关联文件名字
        file.setFileName(path);

        bool isok = file.open(QIODevice::WriteOnly);
        if(isok)
        {
            QString str = ui->textEdit->toPlainText();
            //QString -> QByteArray
            file.write(str.toUtf8());
            file.close();
        }

    }
}

```

## 获取文件的信息
QFileInfo  F1
```c
QFileInfo info(path);
info.fileName().toUtf8().data(); //显示utf8的
```

## QDataStream
数据流（二进制）


## QTextStream
setCodec() // 按编码保存

## QBuffer
`#include <QBuffer>  // 内存文件` 

memFile.buffer();//取出来
