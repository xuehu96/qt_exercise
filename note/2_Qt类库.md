# Qt 类库

## 元对象系统
- QObject::metaObject()
```
QObject *obj = new QPushButton;
obj->metaObject()->classname();// 返回QPushButton
```

- QObject::inherits(const chaar * className);  
判断一个对象实例是否是className的类
```
QTimer *timer = new QTimer;
timer->inherits("QTimer");//true
timer->inherits("QAbsractButton");//false
```

- 动态映射 dynamic cast
```
QObject *obj = new QMyWidget;
QWidget *Widget = qobject_cast<QWidget *>(obj); // ok myWidget是Widget的子类
QMyWidget *myWidget = qobject_cast<QMyWidget *>(obj); // ok
```


## 容器类
> 顺序容器 关联容器  

### 顺序容器类
- QList
数组、前后添加快，可以下标访问at()  
insert replace removeAt move swap append prepend removeFirst removeLast isEmpty size

- QLinkedList
链表、基于迭代器访问、不提供下标发昂文

- QVector
动态数组 性能高

- QStack
栈 LIFO  
push pop

- QQueue
队列 FIFO  
enqueue dequeue

### 关联容器类
- QMap
```
QMap<Key,T>

QMap<QString,int> map;
map["one"] = 1;
map["two"] = 2;

map.insert("four",4);
map.remove("two");

//查找
timeout = map.value("TIMEOUT",30);//30是缺省值

```
- QMultiMap
- QHash
- QMultiHash
- QSet
集合 查找快 内部是QHash  
set.contains("cat")

### 容器的迭代
- java迭代器
- STL迭代器
- foreach(宏)

#### java迭代器
| 容器                              | 只读迭代器             | 可读写迭代器                  |
|-----------------------------------|------------------------|-------------------------------|
| QList<T>, QQueue<T>               | QListIterator<T>       | QMutableListIterator<T>       |
| QLinkedList<T>                    | QLinkedListIterator<T> | QMutableLinkedListIterator<T> |
| QVector<T>, QStack<T>             | QVectorIterator<T>     | QMutableVectorIterator<T>     |
| QSet<T>                           | QSetIterator<T>        | QMutableSetIterator<T>        |
| QMap<Key, T>, QMultiMap<Key, T>   | QMapIterator<Key, T>   | QMutableMapIterator<Key, T>   |
| QHash<Key, T>, QMultiHash<Key, T> | QHashIterator<Key, T>  | QMutableHashIterator<Key, T>  |

#### QListIterator的API：

|      函数      |                   用途                   |
|:--------------:|:----------------------------------------:|
|    toFront()   | 将迭代器移到list的最前面(在第一个项之前) |
|    toBack()    |  将迭代器移到list的最后面 (最后一项之后) |
|    hasNext()   |   如果迭代器没有到list的最后则返回true   |
|     next()     |  返回下一项，并将迭代器向前移动一个位置  |
|   peekNext()   |        返回下一项，不会移动迭代器        |
|  hasPrevious() |  如果迭代器没有到list的最前面则返回true  |
|   previous()   |   返回上一项，并将迭代器移到上一个位置   |
| peekPrevious() |        返回上一项，不会移动迭代器        |

#### STL风格的迭代器

| 容器                              | 只读迭代器                     | 可读写的迭代器           |
|-----------------------------------|--------------------------------|--------------------------|
| QList<T>, QQueue<T>               | QList<T>::const_iterator       | QList<T>::iterator       |
| QLinkedList<T>                    | QLinkedList<T>::const_iterator | QLinkedList<T>::iterator |
| QVector<T>, QStack<T>             | QVector<T>::const_iterator     | QVector<T>::iterator     |
| QSet<T>                           | QSet<T>::const_iterator        | QSet<T>::iterator        |
| QMap<Key, T>, QMultiMap<Key, T>   | QMap<Key, T>::const_iterator   | QMap<Key, T>::iterator   |
| QHash<Key, T>, QMultiHash<Key, T> | QHash<Key, T>::const_iterator  | QHash<Key, T>::iterator  |

#### STL风格迭代器的API：
| 表达式 | 用途                       |
|:------:|----------------------------|
|   *i   | 返回当前项                 |
|   ++i  | 将迭代器指向下一项         |
| i += n | 迭代器向前移动n项          |
|   --i  | 将迭代器指向上一项         |
| i -= n | 将迭代器你向后移动n项      |
|  i - j | 返回迭代器i和j之间项的数目 |

#### foreach关键字  
```c
/////foreach遍历QLinkedList<QString>
QLinkedList<QString> list;
...
QString str;
foreach (str, list)
    qDebug() << str;

/////QMap和QHash中
QMap<QString, int> map;
...
foreach (const QString &str, map.keys())
    qDebug() << str << ":" << map.value(str);

/////对于一个多值的（multi-valued）map
QMultiMap<QString, int> map;
...
foreach (const QString &str, map.uniqueKeys()) 
{
    foreach (int i, map.values(str))
        qDebug() << str << ":" << i;
}
```

## QString 类
- 转换到整数  
toInt  toLong toShort toUInt toULong
- 转换到浮点数
toDouble toFloat
- 进制转换
```
QString str = ui->editDec->text();
int val = str.toInt();//10进制
str = QString::number(val,16);//16进制
str = str.setNum(val,16);
str = str.toUpper();
str = str.setNum(val,2);
//str = QString::number(val,2);
```

### 组字符串 arg
```
QString str = QString("%1  %2").arg("Mike").arg(123);
```

### QString常用功能
- append()  prepend()前面添加
- toUpper toLower
- count size length 功能相同 汉字算一个
- trimmed 去掉最后空格 ， simplified 多个空格用一个替换
- indexOf lastIndexOf 出现位置
- isNull(未初始化)  **isEmpty**(只有\0是true, 常用)
- contains() 包含 
- endsWith startsWith 判断是否某个结尾
- left 和 right 从左右取出字符
- section 分割

### QString 和 QByteArray
```c++
//QString -> QByteArray
QString str("hello");  
QByteArray bytes = str.toUtf8(); // QString转QByteArray方法1
bytes.toLocal8Bits();//本地编码 ANSI

//QByteArray -> char *
char *b = a.data();

//QString -> char*
str.toUtf8().data()

//char * -> QString
QString c = QString(p);
```


## 时间和日期类
- QTime 时间 21:46:58
- QDate 日期 2019-11-1
- QDataTime 日期时间 2019-11-1 21:47:35
时间转化为字符串
```c
#include <QDateTime>
    QDateTime curDataTime = QDateTime::currentDateTime();
    ui->lineEdit->setText(curDataTime.toString("yyyy-MM-dd hh:mm:ss:zzz A"));
```

字符串转化为时间
datetime = QDateTime::fromString(str,"yyyy-MM-dd hh:mm:ss");



## 定时器类 QTimer
```c
/////.h
private:
    QTimer *timer;

private slots:
    void on_timer_timeout();

/////.cpp
timer = new QTimer(this);

connect(timer, SIGNAL(timeout()), this, SLOT(on_timer_timeout()));
timer->start(1000);

```