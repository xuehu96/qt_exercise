# Qt TCP
>服务器有 两个套接字 ，一个监听 一个传输

如果项目用到网络，需要添加  
`QT+=network`

## Linux Server 端
bind listen accept  
read/write

## Linux client 端
connect  
read/write

## QT Server 端 -QTcpServer
listen()【与bind合并】   
//如果连接成功服务器会触发newConnection()信号  
槽函数->取出建立好连接的套接字  类型：QTcpSocket  
如果收到消息，通信套接字会受到readyRead()需要在对应槽函数做接收处理  
read/tcp.write()

## QT client 端 - QTcpSocket
connectToHost()  
如果连接成功会触发connected()信号，不管服务器还是客户端  
如果对方主动断开连接，会触发disconnected()信号  
read/write

## server
```c++
tcpServer = new QTcpServer(this);

tcpServer->listen(QHostAddress::any,8888);

connect(tcpServer,&QTcpServer::newCOnnection,
    [=]()
    {
        tcpSocket = tcpServer->nextPendingCOnnection();
        QString ip=tcpSocket.peerAddress().toString();
        qint16 port=tcpSocket.peerPort();

        QString temp = QString("[%1:%2]:成功连接").arg(ip).arg(port);
    }
);
```

## client
```c++

```
## 定时器对象QTimer
```c++
/// .h
#include <QTimer>

private:
QTimer *myTimer;
/// .cpp
myTimer = new QTimer(this);
//定时器有个信号singal timeout()
connect(myTimer,&QTimer::timeout,
    [=]()
    {
        static int i = 0;
        i++;
        ui->lcdNumber->display(i);
    }
);

//startBUtton
if(myTimer->isActive() == false)
    myTimer->start(100);//启动定时器，时间间隔为100ms

//stop button
if(myTimer->isActive() == true)
    myTimer->stop();
```

# Qt UDP



# Qt TCP传文件