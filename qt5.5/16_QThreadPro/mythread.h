#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QObject>

class MyThread : public QObject
{
    Q_OBJECT
public:
    explicit MyThread(QObject *parent = 0);

    void MyTimeOut();
    bool setStop(bool flag = true);

signals:
    void timeout();

public slots:

private:
    bool isstop;
};

#endif // MYTHREAD_H
