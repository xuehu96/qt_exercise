#include "mythread.h"
#include <QThread>
#include <QDebug>

MyThread::MyThread(QObject *parent) : QObject(parent)
{
    isstop = false;
}

void MyThread::MyTimeOut()
{

    while(isstop == false)
    {
        QThread::sleep(1);
        emit timeout();
         qDebug()<<"子线程号"<<QThread::currentThread();
    }
}

bool MyThread::setStop(bool flag)
{
    isstop = flag;
}

