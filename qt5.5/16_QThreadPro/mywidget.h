#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include <mythread.h>
#include <QThread>

namespace Ui {
class MyWidget;
}

class MyWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MyWidget(QWidget *parent = 0);
    ~MyWidget();

    void dealTimeOut();

signals:
    void startThread();

private slots:
    void on_button_start_clicked();

    void on_button_st_clicked();

private:
    Ui::MyWidget *ui;
    MyThread *myT;
    QThread *thread;
};

#endif // MYWIDGET_H
