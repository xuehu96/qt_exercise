#include "mywidget.h"
#include "ui_mywidget.h"
#include <QDebug>

MyWidget::MyWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MyWidget)
{
    ui->setupUi(this);

    //创建一个线程对象   不指定父对象，指定父对象之后不能移动
    myT = new MyThread;

    //创建一个QThread对象 指定父对象
    thread = new QThread(this);

    //把自定义线程加入到子线程中
    myT->moveToThread(thread);

    connect(myT,&MyThread::timeout,this,&MyWidget::dealTimeOut);

    qDebug()<<"主线程号"<<QThread::currentThread();

    connect(this,&MyWidget::startThread,myT,&MyThread::MyTimeOut);

    //程序退出的时候退出线程
    connect(this,&MyWidget::destroyed,this,[=](){
        on_button_st_clicked();

        delete myT;//没有指定父对象  delete掉
    });

}

MyWidget::~MyWidget()
{
    delete ui;
}

void MyWidget::dealTimeOut()
{
    static int i = 0;
    i++;
    ui->lcdNumber->display(i);
}

void MyWidget::on_button_start_clicked()
{
    if(thread->isRunning() == true)
    {
        return;
    }
    //启动线程
    myT->setStop(false);
    thread->start(); // 并没有启动线程处理函数

    //不能直接调用myT->myTimeout();
    //只能通过signal slot方式调用 F4定义一个
    emit startThread();
}

void MyWidget::on_button_st_clicked()
{
    //停止线程
    if(thread->isRunning() == false)
    {
        return;
    }

    //但是因为死循环 所以quit比较温柔 杀不死线程
    myT->setStop(true);
    thread->quit();
    thread->wait();
}
