#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>

namespace Ui {
class MyWidget;
}

class MyWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MyWidget(QWidget *parent = 0);
    ~MyWidget();

protected:
    void keyPressEvent(QKeyEvent *event);

    void timerEvent(QTimerEvent *event);

    void mousePressEvent(QMouseEvent *event);

    //窗口关闭事件
    void closeEvent(QCloseEvent *event);

    //重写event事件
    bool event(QEvent *event);

    //重写一下事件过滤器
    bool eventFilter(QObject *watched, QEvent *event);


private:
    Ui::MyWidget *ui;
};

#endif // MYWIDGET_H
