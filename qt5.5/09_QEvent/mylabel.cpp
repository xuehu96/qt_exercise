#include "mylabel.h"
#include <QMouseEvent>
#include <QDebug>

MyLabel::MyLabel(QWidget *parent) : QLabel(parent)
{
    //设置默认追踪鼠标
    this->setMouseTracking(true);
}

void MyLabel::mousePressEvent(QMouseEvent *ev)
{
    int a = ev->x();
    int b = ev->y();

    QString str = QString("<center><h1>press = (%1,%2)</h1></center>").arg(a).arg(b);
    this->setText(str);

    //判断是左键还是右键
    if(ev->button()==Qt::LeftButton)
    {
        qDebug()<<"左键按下";
    }

}

void MyLabel::mouseReleaseEvent(QMouseEvent *ev)
{
    int a = ev->x();
    int b = ev->y();

    QString str = QString("<center><h1>release = (%1,%2)</h1></center>").arg(a).arg(b);
    this->setText(str);


}

void MyLabel::mouseMoveEvent(QMouseEvent *ev)
{
    int a = ev->x();
    int b = ev->y();

    QString str = QString("<center><h1>move = (%1,%2)</h1></center>").arg(a).arg(b);
    this->setText(str);

}

void MyLabel::leaveEvent(QEvent *event)
{
    qDebug()<<"离开";
}

void MyLabel::enterEvent(QEvent *event)
{
    qDebug()<<"进入";
}
