#include "mybutton.h"
#include <QMouseEvent>
#include<QDebug>

MyButton::MyButton(QWidget *parent) : QPushButton(parent)
{

}

void MyButton::mousePressEvent(QMouseEvent *e)
{
    if(e->button() == Qt::LeftButton)//左键按下
    {
        //信号接收
        qDebug()<<"左键被按下";
        //接收完 事件不会继续往下传递


        //不是在这里用的 用在close event
        //e->ignore();//忽略，继续往下传递
                    //忽略给了父组件（不是父子类，是组件）

        //继续传递
        QPushButton::mousePressEvent(e);
    }
    else
    {
        //信号的忽略  交给父类
        QPushButton::mousePressEvent(e);
    }
}
