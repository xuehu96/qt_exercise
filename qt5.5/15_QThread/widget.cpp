#include "widget.h"
#include "ui_widget.h"
#include <QThread>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    mytimer = new QTimer(this);

    connect(mytimer,&QTimer::timeout,this,&Widget::dealTimeout);

    thread = new MyThread(this);

    connect(thread,&MyThread::finish,this,[=](){
        mytimer->stop();
    });

    //关闭窗口的时候销毁线程
    connect(this,&Widget::destroyed,this,[=](){
        //停止线程
        thread->quit();
        thread->wait();//回收
    });

}

Widget::~Widget()
{
    delete ui;
}

void Widget::dealTimeout()
{
    static int i = 0;
    i++;
    ui->lcdNumber->display(i);
}

void Widget::on_pushButton_clicked()
{
    if(mytimer->isActive() == false)
    {
        mytimer->start(100);
    }

    thread->start();

    //mytimer->stop();

}
