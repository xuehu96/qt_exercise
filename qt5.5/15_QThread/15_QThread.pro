#-------------------------------------------------
#
# Project created by QtCreator 2019-12-24T22:08:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 15_QThread
TEMPLATE = app


SOURCES += main.cpp \
        widget.cpp \
    mythread.cpp

HEADERS  += widget.h \
    mythread.h

FORMS    += widget.ui

CONFIG += C++11
