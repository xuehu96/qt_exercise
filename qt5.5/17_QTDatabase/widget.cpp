#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    //打印Qt支持的数据库驱动
    qDebug()<<QSqlDatabase::drivers();

    //添加Mysql数据库
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("127.0.0.1");
    db.setPort(10026);
    db.setUserName("root");
    db.setPassword("123");
    db.setDatabaseName("test"); //使用哪个数据库

    if(db.open() == false)
    {
        QMessageBox::warning(this,"错误","连接失败"+db.lastError().text());
    }
    else
    {
        QMessageBox::information(this,"提示","连接成功");

        //QSqlQuery q;
       // q.exec("select * from list;");

    }
}

Widget::~Widget()
{
    delete ui;
}
