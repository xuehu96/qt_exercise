#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCompleter>
#include <QStringList>
#include <QDateTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStringList strlist;
    strlist <<"Hello"<<"world"<<"hehe";

    QCompleter *com = new QCompleter(strlist,this);
    com->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineEdit->setCompleter(com);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QDateTime curDataTime = QDateTime::currentDateTime();
    ui->lineEdit->setText(curDataTime.toString("yyyy-MM-dd hh:mm:ss:zzz A"));
}
