#include "widget.h"
#include "ui_widget.h"
#include <QPainter> //画家
#include <QPen> //画笔
#include <QBrush> //画刷

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    x = 0;
}

Widget::~Widget()
{
    delete ui;
}

void Widget::paintEvent(QPaintEvent *)
{
    //QPainter p(this);

    QPainter p;
    p.begin(this); //指定当前窗口绘图设备
    //绘图操作
    //  p.drawxxx()

    //画背景图
    p.drawPixmap(0,0,width(),height(),QPixmap("../1.jpg"));

    //画直线
    p.drawLine(50,50,150,50);
    p.drawLine(50,50,50,150);

    //定义画笔
    QPen pen;
    pen.setWidth(5);
//    pen.setColor(Qt::red);
    pen.setColor(QColor(14,9,234));
    pen.setStyle(Qt::DashLine);
    p.setPen(pen);// 把画笔交给画家
    p.drawLine(60,60,150,60);
    p.drawLine(60,60,60,150);

    //画矩形
    //左上角x y  长度 宽度
    p.drawRect(150,150,100,50);

    //画圆
    p.drawEllipse(QPoint(150,150),50,25);

    //封闭图形可以设置画刷
    QBrush brush;
    brush.setColor(Qt::red);
    brush.setStyle(Qt::Dense1Pattern);

    p.setBrush(brush);//画刷交给画家
    p.drawRect(250,250,100,50);

    //画个猫
    p.drawPixmap(x,200,80,80,QPixmap("../1.jpg"));

    p.end();
}

void Widget::on_pushButton_clicked()
{
    x +=20;
    if(x > width())
        x = 0;
    //刷新窗口
    update(); // 间接调paintevent
}
