#include <QApplication>
#include <QWidget>
#include <QPushButton>

int main(int argc,char **argv)
{
    QApplication app(argc,argv);

    QWidget w;
    w.setWindowTitle("Hello Qt");

    //如果不指定父对象，窗口和窗口没有关系，独立
    //指定父对象有2种方法 setParent  通过构造函数
    QPushButton b;
    b.setText("^_^");//设置按钮内容
    b.setParent(&w);


    w.show();

    app.exec();
    return 0;
}
