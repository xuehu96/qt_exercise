#include "mainwindow.h"

#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QDebug>

#include <QDialog>

#include <QFileDialog>

//标准对话框
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QMenuBar *mbar = menuBar();
    setMenuBar(mbar);
    QMenu *menu = mbar->addMenu("对话框");

    QAction *p1 = menu->addAction("模态对话框");

    connect(p1,&QAction::triggered,
        [](){
        QDialog dlg;
        dlg.exec();
    });

    QAction *p2 = menu->addAction("非模态对话框");

    connect(p2,&QAction::triggered,
        [=](){
        //QDialog *dlg = new QDialog(this);
        //dlg->show();

        QDialog *dlg = new QDialog;
        dlg->setAttribute(Qt::WA_DeleteOnClose);
        dlg->show();
    });

    QAction *p3 = menu->addAction("关于对话框");

    connect(p3,&QAction::triggered,
        [=](){
        QMessageBox::about(this,"about","关于Qt");
    });


    QAction *p4 = menu->addAction("问题对话框");

    connect(p4,&QAction::triggered,
        [=](){
        int ret = QMessageBox::question(this,"question","Are you ok?");
//        int ret = QMessageBox::question(this,"question","Are you ok?",
//                                      QMessaageBox::Ok|QMessageBox::Cancel);
        switch(ret)
        {
        case QMessageBox::Yes:
            qDebug()<<"Yes";
            break;
        case QMessageBox::No:
            qDebug()<<"NO";
            break;
        }
    });


    QAction *p5 = menu->addAction("文件对话框");

    connect(p5,&QAction::triggered,
        [=](){
        QString path = QFileDialog::getOpenFileName(
                    this,
                    "open",
                    "../",
                    "souce(*.cpp *.h);;Text(*.txt);;All(*.*)"
                    );
        qDebug()<<path;
    });
}

MainWindow::~MainWindow()
{

}
