#include "mywidget.h"

//应用程序类
//Qt 头文件没有.h  有文件和类名一样
//Q开头 前两个字母大写
#include <QApplication>

int main(int argc, char *argv[])
{
    //有且只有一个应用程序类的对象
    QApplication a(argc, argv);

    //MyWidget 集成QWidget
    //w是一个窗口
    MyWidget w;
    w.show();

    //等待事件发生
    return a.exec();
}
