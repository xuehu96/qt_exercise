#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    this->setStyleSheet("QLabel{color:rgb(0,255,255);"
                        "background-image:url(://img.jpg);"
                        "}"
                        "QRadioButton{"
                        "background-color:red;"
                        "}"
                        "QRadioButton:hover{"
                        "background-color:green;"
                        "}");
    ui->label->setStyleSheet("QLabel{color:rgb(0,255,255);"
                             "background-color:red"
                             "}");

    ui->pushButton->setStyleSheet("QPushButton{"
                                  "background-color:red;"
                                  "}"
                                  "QPushButton:hover{"
                                  "background-color:blue;"
                                  "}"
                                  "QPushButton:pressed{"
                                  "background-color:green;"
                                  "}");
}

Widget::~Widget()
{
    delete ui;
}
