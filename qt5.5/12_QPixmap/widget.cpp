#include "widget.h"
#include "ui_widget.h"
#include <QPainter>
#include <QPicture>
#include <QPixmap>
#include <QImage>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    ///////////////QPixmap//////////////
    //创建绘图设备 400x300
    QPixmap pixmap(400,300);
    //北京填充白色
    pixmap.fill(Qt::white);
    QPainter p(&pixmap);
    //背景填充白色
    p.fillRect(0,0,400,300,QBrush(Qt::white));
    p.drawPixmap(0,0,80,80,QPixmap("../1.jpg"));
    p.drawLine(0,0,40,40);
    pixmap.save("../2.png");

    ////////////QImage////////////////
//    QImage(int width, int height, Format format)
    QImage image(400,300,QImage::Format_ARGB32/*背景透明*/);
    QPainter p2;
    p2.begin(&image);
    p2.drawImage(0,0,QImage("../1.jpg"));
    p2.end();

    //对像素点进行操作
    for(int i = 0; i<50; i++)
        for(int j = i;j<50;j++)
        {
            image.setPixel(QPoint(i,j),qRgb(0,255,0));
//            QRgb image.pixel(QPoint(i,j));// 获取qRgb
        }

    image.save("../3.png");

    ///////////QPicture////////////
    QPicture picture;
    QPainter p3;
    p3.begin(&picture);
    p3.drawPixmap(0,0,80,80,QPixmap("../1.jpg"));
    p3.drawLine(0,0,40,40);
    p3.end();
    picture.save("../picture.png");

    //pixmap转image
    pixmap.load("../1.jpg");
    QImage tempImage = pixmap.toImage();
    //image转 QPiaxmap
    QPixmap tempPixmap = QPixmap::fromImage(tempImage);


}
void Widget::paintEvent(QPaintEvent *)
{

    //本地加载
    QPicture pic;
    pic.load("../picture.png");
    QPainter p(this);
    p.drawPicture(0,0,pic);
}

Widget::~Widget()
{
    delete ui;
}
