#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    writedata();
    readdata();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::writedata()
{
    QFile file("../test.txt");

//    打开文件
    bool ok = file.open(QIODevice::WriteOnly);
    if(ok)
    {
        //创建数据流，和file文件关联
        //向数据流输入数据，相当于往文件写数据
        QDataStream stream(&file);

        stream <<QString("哈哈哈")<<2560;
        file.close();
    }
}

void Widget::readdata()
{
    QFile file("../test.txt");

//    打开文件
    bool ok = file.open(QIODevice::ReadOnly);
    if(ok)
    {
        //创建数据流，和file文件关联
        //向数据流输入数据，相当于往文件写数据
        QDataStream stream(&file);

        QString str;
        int a;
        stream >> str>>a;
        qDebug()<<str.toUtf8().data()<<"a:"<<a;
        ui->textEdit->setText(str.toUtf8().data());
        file.close();
    }

}
