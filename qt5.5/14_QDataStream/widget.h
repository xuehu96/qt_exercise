#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDataStream>
#include <QFile>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void writedata();
    void readdata();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
