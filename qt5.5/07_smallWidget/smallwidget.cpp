#include "smallwidget.h"
#include <QSpinBox>
#include <QSlider>
#include <QHBoxLayout>

SmallWidget::SmallWidget(QWidget *parent) : QWidget(parent)
{
    QSpinBox *spin = new QSpinBox(this);
    QSlider *slider = new QSlider(Qt::Horizontal,this);
    QHBoxLayout *hLayout = new QHBoxLayout;//(this);
    hLayout->addWidget(spin);
    hLayout->addWidget(slider);
    setLayout(hLayout); // 没有指定父对象 需要设置

    connect(spin,static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),slider,&QSlider::setValue);
    connect(slider,&QSlider::valueChanged,spin,&QSpinBox::setValue);
}
