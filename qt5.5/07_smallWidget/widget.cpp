#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    ui->labelImage->setPixmap(QPixmap("://img.jpg"));
    ui->labelImage->setScaledContents(true);

}

Widget::~Widget()
{
    delete ui;
}
