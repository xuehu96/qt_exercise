# qt_exercise

## qt5.9

## qt5.12

## qt5.5
Qt是应用程序框架

## qt快捷键
```
界面控制
    F1查看帮助  Esc回到代码
    F2 跳转到函数定义（和Ctrl+鼠标左键一样的效果）
        Shift+F2 声明和定义之间切换
    F4头文件和源文件之间切换
    Ctrl+H diff
    Alt+左右 切换最近文件（UI下Alt+←可以直接到代码）
    Ctrl+Tab 快速切换最近文件
    Ctrl+W 关闭当前文件

代码控制
    Ctrl+ A I 自动整理
    Ctrl+/注释行，取消注释行
    Ctrl + F 查找
    Ctrl+[]  调到块的首位
    F3 查找下一个 Shift + F3 查找上一个
    
    Ctrl + Shift + Up 将当前行的代码向上移动一行
    Ctrl + Shift + Down 将当前行的代码向下移动一行

    Ctrl+Alt+ 上下 复制一行
    Ctrl+Enter 下一行
    Ctrl+Shift+Enter 上一行

    按着Alt shift鼠标

    在头文件里按 Alt+Enter，再按回车键将在cpp中添加对应的方法实体

编译控制
    Ctrl+B编译工程
    Ctrl+R运行工程

F5开始调试
    Shift+F5停止调试
    F9设置和取消断点
    F10 单步前进
    F11 单步进入函数
    Shift + F11单步跳出函数
```