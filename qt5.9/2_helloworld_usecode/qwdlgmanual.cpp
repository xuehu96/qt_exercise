#include "qwdlgmanual.h"
#include <QFont>
#include <QDebug>
#include <QPalette>

QWDlgManual::QWDlgManual(QWidget *parent)
    : QDialog(parent)
{
    initUI();
    initSignalSlot();
}

QWDlgManual::~QWDlgManual()
{
}

void QWDlgManual::initUI()
{
    mainLayout = new QVBoxLayout(this);
    //checkbox
    chkBold = new QCheckBox("Blod",this);
    chkItalic = new QCheckBox("Italic",this);
    chkUnder = new QCheckBox("Under",this);

    chkLayout = new QHBoxLayout(this);
    chkLayout->addWidget(chkBold);
    chkLayout->addWidget(chkItalic);
    chkLayout->addWidget(chkUnder);

    mainLayout->addLayout(chkLayout);

    //radiobutton
    radioBtnBlack = new QRadioButton("Black",this);
    radioBtnRed = new QRadioButton("Red",this);
    radioBtnBlue = new QRadioButton("Blue",this);

    radioLayout = new QHBoxLayout(this);
    radioLayout->addWidget(radioBtnBlack);
    radioLayout->addWidget(radioBtnRed);
    radioLayout->addWidget(radioBtnBlue);
    mainLayout->addLayout(radioLayout);

    //textEdit
    textEdit = new QPlainTextEdit(this);
    textEdit->setPlainText("Helloworld\nIt is my demo");
    QFont font = textEdit->font();
    font.setPointSize(20);
    textEdit->setFont(font);
    mainLayout->addWidget(textEdit);


    //button
    btnOK = new QPushButton("确定",this);
    btnCancel = new QPushButton("取消",this);
    btnClose = new QPushButton("退出",this);
    btnLayout = new QHBoxLayout(this);
    btnLayout->addWidget(btnOK);
    btnLayout->addWidget(btnCancel);
    btnLayout->addWidget(btnClose);

    mainLayout->addLayout(btnLayout);
    //main
    setLayout(mainLayout);
}

void QWDlgManual::initSignalSlot()
{
    //slot function
    connect(chkBold,&QCheckBox::clicked,this,&QWDlgManual::setTextBold);
    //lambda
    connect(chkItalic,&QCheckBox::clicked,this,[=](bool checked)
    {
        QFont font = textEdit->font();
        font.setItalic(checked);
        textEdit->setFont(font);
    });

    connect(chkUnder,&QCheckBox::clicked,this,[=](bool checked)
    {
        QFont font = textEdit->font();
        font.setUnderline(checked);
        textEdit->setFont(font);
    });

    //设置颜色
    connect(radioBtnRed,&QRadioButton::clicked,this,&QWDlgManual::setTextColor);
    connect(radioBtnBlack,&QRadioButton::clicked,this,&QWDlgManual::setTextColor);
    connect(radioBtnBlue,&QRadioButton::clicked,this,&QWDlgManual::setTextColor);

    //按钮
    connect(btnOK,&QPushButton::clicked,this,&QWDlgManual::accept);
    connect(btnCancel,&QPushButton::clicked,this,&QWDlgManual::reject);
    connect(btnClose,&QPushButton::clicked,this,&QWDlgManual::close);
}

void QWDlgManual::setTextBold(bool checked)
{
    QFont font = textEdit->font();
    font.setBold(checked);
    textEdit->setFont(font);
}

void QWDlgManual::setTextColor()
{
    QPalette plet = textEdit->palette();
    if(radioBtnBlue->isChecked())
        plet.setColor(QPalette::Text,Qt::blue);
    else if(radioBtnRed->isChecked())
        plet.setColor(QPalette::Text,Qt::red);
    else if(radioBtnBlack->isChecked())
        plet.setColor(QPalette::Text,Qt::black);
    else
        plet.setColor(QPalette::Text,Qt::black);

    textEdit->setPalette(plet);
}
