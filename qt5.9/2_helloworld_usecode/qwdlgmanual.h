#ifndef QWDLGMANUAL_H
#define QWDLGMANUAL_H

#include <QDialog>
#include <QCheckBox>
#include <QRadioButton>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPlainTextEdit>

class QWDlgManual : public QDialog
{
    Q_OBJECT

public:
    QWDlgManual(QWidget *parent = 0);
    ~QWDlgManual();

private:
    void initUI();
    void initSignalSlot();
private slots:
    void setTextBold(bool checked);
    void setTextColor();

private:
    QCheckBox *chkUnder;
    QCheckBox *chkItalic;
    QCheckBox *chkBold;

    QRadioButton *radioBtnBlack;
    QRadioButton *radioBtnRed;
    QRadioButton *radioBtnBlue;

    QPlainTextEdit *textEdit;

    QPushButton *btnOK;
    QPushButton *btnCancel;
    QPushButton *btnClose;

    QHBoxLayout *chkLayout;
    QHBoxLayout *radioLayout;
    QHBoxLayout *btnLayout;

    QVBoxLayout *mainLayout;

};

#endif // QWDLGMANUAL_H
